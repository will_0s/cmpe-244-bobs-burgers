#pragma once

#include "l3_drivers/gpio.h"
#include <stdint.h>
#include <stdio.h>

void music_player__init(void);

void music_player__loop_mode(uint8_t mode);

void music_player__play(void);
void music_player__song(uint16_t number);
void music_player__stop(void);
void music_player__volume(uint16_t volume);
