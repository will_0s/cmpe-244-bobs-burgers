#include "music__player.h"
#include "clock.h"
#include "l3_drivers/uart.h"
#include <stdint.h>
#include <stdio.h>

static uint8_t music_player__music_player__checksum(uint8_t *uart_commad, uint8_t length) {
  uint8_t sum = 0;
  for (uint8_t i = 0; i < length; i++) {
    sum = sum + uart_commad[i];
  }
  return sum;
}

static void music_player__uart__polled_put(uint8_t *uart_commad, uint8_t length) {
  uint8_t checksum = music_player__music_player__checksum(uart_commad, length);

  for (int i = 0; i < length; i++) {

    bool d = uart__polled_put(UART__3, uart_commad[i]);
    if (d) {
      printf("it was sent %x \n", uart_commad[i]);
    }
  }

  uart__polled_put(UART__3, checksum);
}

void music_player__loop_mode(uint8_t mode) {
  uint8_t uart_commad[4] = {0xaa, 0x18, 0x01, 0x00};
  uart_commad[3] = mode;

  music_player__uart__polled_put(uart_commad, 4);
}

void music_player__volume(uint16_t volume) {
  uint8_t uart_commad[4] = {0xaa, 0x13, 0x01, 0x00};
  uart_commad[3] = volume;

  music_player__uart__polled_put(uart_commad, 4);
}

void music_player__play(void) {
  uint8_t uart_commad[4] = {0xaa, 0x02, 0x00, 0xac};

  for (int i = 0; i < 4; i++) {

    uart__polled_put(UART__3, uart_commad[i]);
  }
}

void music_player__stop(void) {
  uint8_t uart_commad[4] = {0xaa, 0x04, 0x00, 0xae};

  music_player__uart__polled_put(uart_commad, 4);
}

void music_player__song(uint16_t number) {
  uint8_t uart_commad[5] = {0xaa, 0x07, 0x02, 0x00, 0x00};
  uart_commad[4] = number & 0xff;

  music_player__uart__polled_put(uart_commad, 5);
}

void music_player__init(void) {

  gpio__construct_with_function(GPIO__PORT_4, 28, GPIO__FUNCTION_2); //  Tx
  gpio__construct_with_function(GPIO__PORT_4, 29, GPIO__FUNCTION_2); //  Rx

  const uint16_t MODE_INACTIVE = (3 << 3);

  LPC_IOCON->P4_29 &= ~MODE_INACTIVE;
  LPC_IOCON->P4_28 &= ~MODE_INACTIVE;

  uart__init(UART__3, clock__get_peripheral_clock_hz(), 9600);
}