#include "game_objects.h"
#include "led_driver.h"
#include "stdint.h"
#include "stdio.h"

void game_object__block(uint8_t x, uint8_t y, uint8_t block_length, color_code color) {

  int row, col;

  for (row = 0; row < 2; row++) {

    for (col = 0; col < block_length; col++) {

      led_driver__setPixel(row + y, col + x, color);
    }
  }
}
void game_object__ball(uint8_t x, uint8_t y, color_code color) {
  led_driver__setPixel(y, x, 0);
  led_driver__setPixel(y, x + 1, color);
  led_driver__setPixel(y, x + 2, 0);
  led_driver__setPixel(y + 1, x, color);
  led_driver__setPixel(y + 1, x + 1, color);
  led_driver__setPixel(y + 1, x + 2, color);
  led_driver__setPixel(y + 2, x, 0);
  led_driver__setPixel(y + 2, x + 1, color);
  led_driver__setPixel(y + 2, x + 2, 0);
}

void game_object__x(uint8_t x, uint8_t y, color_code color) {

  led_driver__setPixel(y, x, color);
  led_driver__setPixel(y, x + 1, 0);
  led_driver__setPixel(y, x + 2, color);
  led_driver__setPixel(y + 1, x, 0);
  led_driver__setPixel(y + 1, x + 1, color);
  led_driver__setPixel(y + 1, x + 2, 0);
  led_driver__setPixel(y + 2, x, color);
  led_driver__setPixel(y + 2, x + 1, 0);
  led_driver__setPixel(y + 2, x + 2, color);
}

void game_object__circle(uint8_t x, uint8_t y, color_code color) {

  led_driver__setPixel(y, x, color);
  led_driver__setPixel(y, x + 1, color);
  led_driver__setPixel(y, x + 2, color);
  led_driver__setPixel(y + 1, x, color);
  led_driver__setPixel(y + 1, x + 1, 0);
  led_driver__setPixel(y + 1, x + 2, color);
  led_driver__setPixel(y + 2, x, color);
  led_driver__setPixel(y + 2, x + 1, color);
  led_driver__setPixel(y + 2, x + 2, color);
}

void game_object__block_on_the_side(uint8_t x, uint8_t y, uint8_t block_length, color_code color) {

  int row, col;

  for (col = 0; col < 2; col++) {

    for (row = 0; row < block_length; row++) {

      led_driver__setPixel(row + y, col + x, color);
    }
  }
}