#pragma once

#include "l5_application/led_driver/led_driver.h"
#include <stdint.h>
#include <stdio.h>

typedef enum {
  DOWN_RIGHT,
  DOWN_LEFT,
  UP_RIGHT,
  UP_LEFT,
  STAY,
  MIDDLE,

} ball_dir;

void game_object__block(uint8_t x, uint8_t y, uint8_t block_length, color_code color);
void game_object__ball(uint8_t x, uint8_t y, color_code color);
void game_object__x(uint8_t x, uint8_t y, color_code color);
void game_object__circle(uint8_t x, uint8_t y, color_code color);
void game_object__block_on_the_side(uint8_t x, uint8_t y, uint8_t block_length, color_code color);