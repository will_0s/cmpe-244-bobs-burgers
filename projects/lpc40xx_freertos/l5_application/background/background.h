#pragma once

#include "led_driver.h"
#include "stdio.h"

void fill_main_background(void);
void fill_starting_background(void);

void fill_word__game(uint8_t x, uint8_t y, color_code color);
void fill_word__over(uint8_t x, uint8_t y, color_code color);
void fill_word__pause(uint8_t x, uint8_t y, color_code color);
void fill_word__player(uint8_t x, uint8_t y, color_code color);
void fill_word__number_1(uint8_t x, uint8_t y, color_code color);
void fill_word__number_2(uint8_t x, uint8_t y, color_code color);
void fill_word__win(uint8_t x, uint8_t y, color_code color);

void fill_word__game_backwords(uint8_t x, uint8_t y, color_code color);
void fill_word__over_backwords(uint8_t x, uint8_t y, color_code color);
void fill_word__pause_backwords(uint8_t x, uint8_t y, color_code color);
void fill_word__player_backwords(uint8_t x, uint8_t y, color_code color);
void fill_word__number_1_backwords(uint8_t x, uint8_t y, color_code color);
void fill_word__win_backwords(uint8_t x, uint8_t y, color_code color);