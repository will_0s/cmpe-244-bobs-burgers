#include "led_driver.h"
#include "delay.h"
#include <l3_drivers/gpio.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct {
  gpio_s R1;
  gpio_s G1;
  gpio_s B1;
  gpio_s R2;
  gpio_s G2;
  gpio_s B2;
  gpio_s A;
  gpio_s B;
  gpio_s C;
  gpio_s D;
  gpio_s E;
  gpio_s CLK;
  gpio_s OE;
  gpio_s LAT;
} led_matrix_p;

static led_matrix_p pins;

static volatile uint8_t led_matrix_refresh_array[64][64];

void led_driver__matrixInit(void) {
  pins.R1 = gpio__construct_as_output(GPIO__PORT_0, 6);
  pins.G1 = gpio__construct_as_output(GPIO__PORT_0, 7);

  pins.B1 = gpio__construct_as_output(GPIO__PORT_0, 8);

  pins.R2 = gpio__construct_as_output(GPIO__PORT_0, 26);
  pins.G2 = gpio__construct_as_output(GPIO__PORT_0, 9);

  pins.B2 = gpio__construct_as_output(GPIO__PORT_1, 31);
  pins.E = gpio__construct_as_output(GPIO__PORT_2, 4);

  pins.A = gpio__construct_as_output(GPIO__PORT_1, 20);
  pins.B = gpio__construct_as_output(GPIO__PORT_1, 23);

  pins.C = gpio__construct_as_output(GPIO__PORT_1, 28);
  pins.D = gpio__construct_as_output(GPIO__PORT_1, 29);

  pins.CLK = gpio__construct_as_output(GPIO__PORT_2, 0);
  pins.LAT = gpio__construct_as_output(GPIO__PORT_2, 1);

  pins.OE = gpio__construct_as_output(GPIO__PORT_2, 2);
}

static void led_driver__colors_RGB1(color_code color) {
  switch (color) {
  case Black:
    gpio__reset(pins.R1);
    gpio__reset(pins.G1);
    gpio__reset(pins.B1);
    break;
  case Red:
    gpio__set(pins.R1);
    gpio__reset(pins.G1);
    gpio__reset(pins.B1);
    break;
  case Blue:
    gpio__reset(pins.R1);
    gpio__reset(pins.G1);
    gpio__set(pins.B1);
    break;
  case Green:
    gpio__reset(pins.R1);
    gpio__set(pins.G1);
    gpio__reset(pins.B1);
    break;
  case Purple:
    gpio__set(pins.R1);
    gpio__reset(pins.G1);
    gpio__set(pins.B1);
    break;
  case Yellow:
    gpio__set(pins.R1);
    gpio__set(pins.G1);
    gpio__reset(pins.B1);
    break;
  case Cyan:
    gpio__reset(pins.R1);
    gpio__set(pins.G1);
    gpio__set(pins.B1);
    break;
  case White:
    gpio__set(pins.R1);
    gpio__set(pins.G1);
    gpio__set(pins.B1);
    break;
  case Orange:

    gpio__set(pins.R1);
    gpio__reset(pins.B1);
    gpio__set(pins.G1);
    break;
  default:
    gpio__reset(pins.R1);
    gpio__reset(pins.G1);
    gpio__reset(pins.B1);
    break;
  }
}

static void led_driver__colors_RGB2(color_code color) {
  switch (color)

  {
  case Black:
    gpio__reset(pins.R2);
    gpio__reset(pins.G2);
    gpio__reset(pins.B2);
    break;

  case Red:
    gpio__set(pins.R2);
    gpio__reset(pins.G2);
    gpio__reset(pins.B2);
    break;
  case Blue:
    gpio__reset(pins.R2);
    gpio__reset(pins.G2);
    gpio__set(pins.B2);
    break;
  case Green:
    gpio__reset(pins.R2);
    gpio__set(pins.G2);
    gpio__reset(pins.B2);
    break;
  case Purple:
    gpio__set(pins.R2);
    gpio__reset(pins.G2);
    gpio__set(pins.B2);
    break;
  case Yellow:
    gpio__set(pins.R2);
    gpio__set(pins.G2);
    gpio__reset(pins.B2);
    break;
  case Cyan:
    gpio__reset(pins.R2);
    gpio__set(pins.G2);
    gpio__set(pins.B2);
    break;
  case White:
    gpio__set(pins.R2);
    gpio__set(pins.G2);
    gpio__set(pins.B2);
    break;
  case Orange:

    gpio__set(pins.G2);

    gpio__reset(pins.B2);
    gpio__set(pins.R2);

    break;
  default:
    gpio__reset(pins.R2);
    gpio__reset(pins.G2);
    gpio__reset(pins.B2);
    break;
  }
}

static void led_driver__drawRow(uint8_t row) {
  if (row >> 0 & 0x1) {
    gpio__set(pins.A);
  } else {
    gpio__reset(pins.A);
  }
  if (row >> 1 & 0x1) {
    gpio__set(pins.B);
  } else {
    gpio__reset(pins.B);
  }
  if (row >> 2 & 0x1) {
    gpio__set(pins.C);
  } else {
    gpio__reset(pins.C);
  }
  if (row >> 3 & 0x1) {
    gpio__set(pins.D);
  } else {
    gpio__reset(pins.D);
  }
  if (row >> 4 & 0x1) {
    gpio__set(pins.E);
  } else {
    gpio__reset(pins.E);
  }
}

static void led_driver__clock_toggle(void) {
  gpio__set(pins.CLK);
  gpio__reset(pins.CLK);
}
void led_driver__setPixel(uint8_t row, uint8_t col, color_code color) { led_matrix_refresh_array[row][col] = color; }

static void led_driver__insync_row(void) { // draws one more row, testing purposes for even colors
  const uint8_t first_row = 0;
  for (int col = 0; col < 64; col++) {

    led_driver__colors_RGB1(Black);
    led_driver__colors_RGB2(Black);
    led_driver__clock_toggle();
  }

  gpio__set(pins.LAT);
  gpio__set(pins.OE);
  led_driver__drawRow(first_row);
  gpio__reset(pins.LAT);
  gpio__reset(pins.OE);
}

void led_driver__drawBoard(void) {

  int row, col;
  for (row = 0; row < 32; row++) {

    for (col = 0; col < 64; col++) {

      led_driver__colors_RGB1(led_matrix_refresh_array[row][col]);
      led_driver__colors_RGB2(led_matrix_refresh_array[row + 32][col]);
      led_driver__clock_toggle();
    }

    gpio__set(pins.LAT);
    gpio__set(pins.OE);
    led_driver__drawRow(row);
    gpio__reset(pins.LAT);
    gpio__reset(pins.OE);
  }

  led_driver__insync_row();
}

void led_driver__drawBoard_flashing(void) {

  int row, col;
  for (row = 0; row < 32; row++) {
    int random_numb = 1 + rand() % 7;

    for (col = 0; col < 64; col++) {

      if (led_matrix_refresh_array[row][col] == 4) {
        led_driver__colors_RGB1(random_numb);

      } else {
        led_driver__colors_RGB1(led_matrix_refresh_array[row][col]);
      }
      if (led_matrix_refresh_array[row + 32][col] == 4) {

        led_driver__colors_RGB2(random_numb);

      } else {

        led_driver__colors_RGB2(led_matrix_refresh_array[row + 32][col]);
      }
      led_driver__clock_toggle();
    }

    gpio__set(pins.LAT);
    gpio__set(pins.OE);
    led_driver__drawRow(row);
    gpio__reset(pins.LAT);
    gpio__reset(pins.OE);
  }

  led_driver__insync_row();
}

void led_driver__refill_refresh(uint8_t (*update_matrix)[64]) {
  for (int row = 0; row < 64; row++) {

    for (int col = 0; col < 64; col++) {

      led_matrix_refresh_array[row][col] = update_matrix[row][col];
    }
  }
}
