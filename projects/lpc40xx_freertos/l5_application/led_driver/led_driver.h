#pragma once

#include <stdint.h>
#include <stdio.h>

typedef enum {
  Black,
  Red,
  Blue,
  Green,
  Purple,
  Yellow,
  Cyan,
  White,
  Orange,
} color_code;

void led_driver__matrixInit(void);

void led_driver__setPixel(uint8_t row, uint8_t col, color_code color);

void led_driver__drawBoard(void);

void led_driver__drawBoard_flashing(void);

void led_driver__refill_refresh(uint8_t (*new_refresh)[64]);
