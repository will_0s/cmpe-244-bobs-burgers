
#include "FreeRTOS.h"
#include "task.h"

#include "board_io.h"
#include "button.h"
#include "common_macros.h"
#include "joystick.h"
#include "l3_drivers/gpio.h"
#include "led_driver.h"

#include "delay.h"
#include "periodic_scheduler.h"
#include "sj2_cli.h"
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "background.h"
#include "game_objects.h"
#include "main_wrapper.h"
#include "music__player.h"

static void led_screen_task(void *p);
static void ball_task(void *p);
static void score_task(void *p);

static uint8_t state = 0;
static uint8_t start_game = 0;

static uint8_t block_length = 2;

static volatile uint8_t top_block = 0;
static volatile uint8_t bottom_block = 0;

static volatile uint8_t x_ball = 33;
static volatile uint8_t y_ball = 32;

static uint8_t player1_win_icon_col = 1;
static uint8_t player1_win_icon_row = 3;

static uint8_t player2_win_icon_col = 60;
static uint8_t player2_win_icon_row = 3;

static uint8_t player1_goal = 0;
static uint8_t player2_goal = 0;
static uint8_t max_number_of_goals = 3; // 10 is the max number of goals

static volatile uint8_t song_name = 1;

typedef enum {
  Empty,
  intro,         // 20 delay
  post,          // 1
  player1_block, // 1
  player2_blcok, // 1
  goal,          // 2
  whistle,       // 1
  celebration,   // 15

} songs_name;

void wrapping__main(void) {
  board_io__initialize();
  led_driver__matrixInit();
  joystick__initialize();
  button__initialize();
  music_player__init();

  top_block = 32 - block_length / 2;
  bottom_block = 32 - block_length / 2;
  puts("Starting RTOS");

  xTaskCreate(ball_task, "ball_task", 4096 / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  xTaskCreate(led_screen_task, "led_screen_task", 4096 / sizeof(void *), NULL, PRIORITY_MEDIUM, NULL);
  xTaskCreate(score_task, "score task", 4096 / sizeof(void *), NULL, PRIORITY_LOW, NULL);

  vTaskStartScheduler();
}

static int return_state(int c, int r) {

  uint8_t new_state = 0;

  const uint8_t first_post = 20;
  const uint8_t second_post = 40;

  const uint8_t upper_edge = 3;
  const uint8_t lower_edge = 60;

  if (r < upper_edge && (c >= first_post && c <= second_post)) { //  score on the top  block side
    y_ball = 30;
    x_ball = 30;

    music_player__volume(20);
    music_player__song(goal);

    if (player2_goal < max_number_of_goals) {

      player2_goal++;
    }
    new_state = STAY;

  } else if (r > lower_edge && (c >= first_post && c <= second_post)) { // score on bottom block side
    x_ball = 30;
    y_ball = 30;

    music_player__volume(20);
    music_player__song(goal);

    if (player1_goal < max_number_of_goals) {

      player1_goal++;
    }
    new_state = STAY;

  } else if (r >= 58 && ((c >= 6 && c <= 19) || (c >= 41 && c <= 57))) {

    if (state == DOWN_RIGHT) {
      new_state = UP_RIGHT;
    } else {
      new_state = UP_LEFT;
    }
  } else if (r <= 3 && ((c >= 6 && c <= 19) || (c >= 41 && c <= 57))) {

    if (state == UP_RIGHT) {
      new_state = DOWN_RIGHT;
    } else {
      new_state = DOWN_LEFT;
    }
  } else if (r >= 56 &&
             (c >= (bottom_block - 2) && c <= (bottom_block + block_length - 1))) { // hit box of bottom block

    music_player__volume(30);
    music_player__song(player2_blcok);

    if (c < (bottom_block + (int)(block_length / 2) - 2)) {
      new_state = UP_LEFT;
    } else {
      new_state = UP_RIGHT;
    }
  } else if (r <= 5 && (c >= (top_block - 2) && c <= (top_block + block_length - 1))) {

    music_player__volume(30);
    music_player__song(player1_block);

    if (c < (top_block + (int)(block_length / 2) - 2)) {
      new_state = DOWN_LEFT;
    } else {
      new_state = DOWN_RIGHT;
    }
  } else if (c >= 55 && (r <= 56 && r >= 3)) { // bottom floor bounce area

    if (state == DOWN_RIGHT) {
      new_state = DOWN_LEFT;
    } else {
      new_state = UP_LEFT;
    }
  } else if (c <= 6 && (r <= 56 && r >= 3)) { // top floor bounce area

    if (state == DOWN_LEFT) {
      new_state = DOWN_RIGHT;
    } else {
      new_state = UP_RIGHT;
    }
  } else if ((x_ball <= 4 || x_ball >= 59) || (y_ball <= 1 || y_ball >= 62)) { // incase if it falls out of bounce or if
                                                                               // it gets lost return to middle x_ball =

    x_ball = c;
    y_ball = r;
    new_state = MIDDLE;

  } else if ((c == 6 && r == 3) || (c == 6 && r == 58) || (c == 55 && r == 3) || (c == 55 && r == 58)) {

    if (c < 7) {
      x_ball = 9;
      y_ball = top_block + block_length / 2;
      new_state = DOWN_RIGHT;
    } else {
      x_ball = 54;
      y_ball = bottom_block + block_length / 2;
      new_state = DOWN_LEFT;
    }
  } else {
    new_state = state;
  }

  return new_state;
}

static void led_screen_task(void *p) {

  const uint8_t top_edge = 6;
  const uint8_t bottom_edge = 58;

  while (1) {

    if (start_game < 1) {
      x_ball = 31;
      y_ball = 30;
      state = STAY;

      if ((xTaskGetTickCount() % 20300 < 100) || song_name == intro) {

        song_name = celebration;
        music_player__volume(20);
        music_player__song(intro);
      }

      fill_starting_background();
      if ((xSemaphoreTake(switch0_pressed_signal, 0) == pdTRUE) ||
          (xSemaphoreTake(switch1_pressed_signal, 0) == pdTRUE)) {
        start_game++;
      }
      led_driver__drawBoard_flashing();

    } else {

      fill_main_background();

      if ((bottom_block >= top_edge) && (bottom_block <= (bottom_edge - block_length))) {
        bottom_block -= (int)(top_edge * joystick__get_reading(JOYSTICK__1)) / 4;
      } else if (bottom_block < top_edge) {
        bottom_block = top_edge;
      } else if (bottom_block > (bottom_edge - block_length)) {
        bottom_block = (bottom_edge - block_length);
      }

      if ((top_block >= top_edge) && (top_block <= (bottom_edge - block_length))) {
        top_block += (int)(top_edge * joystick__get_reading(JOYSTICK__2) / 4);
      } else if (top_block < top_edge) {
        top_block = top_edge;
      } else if (top_block > (bottom_edge - block_length)) {
        top_block = (bottom_edge - block_length);
      }

      game_object__block(top_block, 3, block_length, Purple);
      game_object__block(bottom_block, 59, block_length, Yellow);

      vTaskDelay(10);

      led_driver__drawBoard();
    }
  }
}

void ball_task(void *p) {
  int movement_paddle_top = (int)(joystick__get_reading(JOYSTICK__1));
  int movement_paddle_bottom = (int)(joystick__get_reading(JOYSTICK__2));

  while (1) {
    if (player1_goal != max_number_of_goals && player2_goal != max_number_of_goals) {
      if (xSemaphoreTake(switch0_pressed_signal, 0) == pdTRUE) {
        // pause till button is pressed again
        start_game = 1;
        while (true) {
          fill_word__pause(4, 24, 4);

          if (xSemaphoreTake(switch0_pressed_signal, 0)) {
            break;
          }
        }

      } else {

        switch (state) {
        case DOWN_RIGHT:
          x_ball += 1;
          y_ball += 1;
          state = return_state(x_ball, y_ball);
          break;
        case DOWN_LEFT:
          x_ball -= 1;
          y_ball += 1;
          state = return_state(x_ball, y_ball);
          break;
        case UP_RIGHT:
          x_ball += 1;
          y_ball -= 1;
          state = return_state(x_ball, y_ball);
          break;
        case UP_LEFT:
          x_ball -= 1;
          y_ball -= 1;
          state = return_state(x_ball, y_ball);
          break;
        case STAY:
          state = STAY;
          if ((movement_paddle_top == (int)ceil(joystick__get_reading(JOYSTICK__2))) &&
              (movement_paddle_bottom == (int)ceil(joystick__get_reading(JOYSTICK__1)))) {
            state = rand() % 4;

            music_player__volume(30);
            music_player__song(whistle);
          }
          break;
        case MIDDLE:
          x_ball = 31;
          y_ball = 30;
          game_object__x(10, 10, Red);
          state = MIDDLE;
          break;
        default:
          x_ball = 10;
          y_ball = 6;
          state = DOWN_RIGHT;
          break;
        }

        game_object__ball(x_ball, y_ball, White);

        movement_paddle_top = (int)(joystick__get_reading(JOYSTICK__1));
        movement_paddle_bottom = (int)(joystick__get_reading(JOYSTICK__2));
      }
    }
    vTaskDelay(17);
  }
}
void score_task(void *p) {

  while (1) {

    if (xSemaphoreTake(switch1_pressed_signal, 0) == pdTRUE) { // reset game
      player2_goal = 0;
      player1_goal = 0;
      x_ball = 30;
      y_ball = 30;
      top_block = 32 - block_length / 2;
      bottom_block = 32 - block_length / 2;

      start_game = 1;     // change this to zero if reset sends you to start game screen
      state = rand() % 4; // randomize first state
      song_name = celebration;
    } else {

      for (size_t r = 0; r < player2_goal; r++) {
        if (player2_goal < max_number_of_goals) {
          game_object__circle(player2_win_icon_col, player2_win_icon_row + (6 * r), Yellow);
        } else {
          game_object__circle(player2_win_icon_col, player2_win_icon_row + (6 * r), Yellow);

          if (song_name == celebration) {
            song_name = Empty;
            music_player__volume(20);
            music_player__song(celebration);
          }

          fill_word__game(22, 15, 1);
          fill_word__over(22, 25, 1);

          fill_word__player(14, 35, Yellow);
          fill_word__number_2(46, 35, Yellow);
          fill_word__win(20, 43, Yellow);
        }
      }
      for (size_t d = 0; d < player1_goal; d++) {

        if (player1_goal < max_number_of_goals) {
          game_object__circle(player1_win_icon_col, player1_win_icon_row + (6 * d), Purple);
        } else {
          game_object__circle(player1_win_icon_col, player1_win_icon_row + (6 * d), Purple);

          if (song_name == celebration) {
            song_name = Empty;
            music_player__volume(20);
            music_player__song(celebration);
          }

          fill_word__game_backwords(22, 43, 1);
          fill_word__over_backwords(22, 35, 1);

          fill_word__player_backwords(23, 25, Purple);
          fill_word__number_1_backwords(14, 25, Purple);
          fill_word__win_backwords(20, 15, Purple);
        }
      }
    }
  }
}
