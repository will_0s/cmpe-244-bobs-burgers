
#pragma once

typedef enum { JOYSTICK__1, JOYSTICK__2 } joystick__number;

// must be called before any uses of joystick__get_reading().
void joystick__initialize(void);

// returns a decimal value between -1 and +1 represendting joystick position in precentage.
float joystick__get_reading(joystick__number joystick);