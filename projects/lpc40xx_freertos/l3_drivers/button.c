#include "button.h"

#include "lpc40xx.h"
#include "lpc_peripherals.h"

#include "gpio.h"
#include "hw_timer.h"
#include "stdio.h"

void sw5_isr(void) {
  fprintf(stderr, "sw5\n");

  xSemaphoreGiveFromISR(switch0_pressed_signal, NULL);
}
void sw6_isr(void) {
  fprintf(stderr, "sw6\n");
  xSemaphoreGiveFromISR(switch1_pressed_signal, NULL);
}

typedef void (*function_pointer_t)(void);

static function_pointer_t button_callbacks_rising_edge[32];
static function_pointer_t button_callbacks_falling_edge[32];

typedef enum {
  GPIO_INTR__FALLING_EDGE,
  GPIO_INTR__RISING_EDGE,
} gpio_interrupt_e;

void button__attach_interrupt(uint32_t pin, gpio_interrupt_e interrupt_type, function_pointer_t callback) {

  if (interrupt_type == GPIO_INTR__RISING_EDGE) {
    LPC_GPIOINT->IO2IntEnR |= (1 << pin);
    button_callbacks_rising_edge[pin] = callback;
  } else if (interrupt_type == GPIO_INTR__FALLING_EDGE) {
    LPC_GPIOINT->IO2IntEnF |= (1 << pin); // Enable P_030 interrupts falling edge
    button_callbacks_falling_edge[pin] = callback;
  }
}

uint32_t getBitPosition(uint32_t bits) {
  uint32_t position = 0;
  while (bits) {
    bits = bits >> 1;
    position++;
  }
  return position - 1;
}

void button__interrupt_dispatcher(void) {
  // Check which pin generated the interrupt
  fprintf(stderr, "isr\n");
  uint32_t risingEdgeInterruptStatus = LPC_GPIOINT->IO2IntStatR;
  uint32_t fallingEdgeInterruptStatus = LPC_GPIOINT->IO2IntStatF;
  if (risingEdgeInterruptStatus) {
    LPC_GPIOINT->IO2IntClr = risingEdgeInterruptStatus;
    const int pin_that_generated_interrupt = getBitPosition(risingEdgeInterruptStatus);
    function_pointer_t attached_user_handler = button_callbacks_rising_edge[pin_that_generated_interrupt];
    // Invoke the user registered callback, and then clear the interrupt
    attached_user_handler();
  } else if (fallingEdgeInterruptStatus) {
    LPC_GPIOINT->IO2IntClr = fallingEdgeInterruptStatus;
    const int pin_that_generated_interrupt = getBitPosition(fallingEdgeInterruptStatus);
    function_pointer_t attached_user_handler = button_callbacks_falling_edge[pin_that_generated_interrupt];
    // Invoke the user registered callback, and then clear the interrupt
    attached_user_handler();
  }
}

void button__initialize(void) {

  switch0_pressed_signal = xSemaphoreCreateBinary();
  switch1_pressed_signal = xSemaphoreCreateBinary();

  // LPC_GPIO2->DIR &= ~(1 << 30);

  lpc_peripheral__enable_interrupt(LPC_PERIPHERAL__GPIO, button__interrupt_dispatcher, "gpio interrupt");

  NVIC_EnableIRQ(GPIO_IRQn); // Enable interrupt gate for the GPIO

  LPC_IOCON->P2_5 &= ~(3 << 3);
  LPC_IOCON->P2_6 &= ~(3 << 3);

  LPC_IOCON->P2_5 |= (1 << 3);
  LPC_IOCON->P2_6 |= (1 << 3);

  button__attach_interrupt(5, GPIO_INTR__RISING_EDGE, sw5_isr);
  button__attach_interrupt(6, GPIO_INTR__RISING_EDGE, sw6_isr);
}