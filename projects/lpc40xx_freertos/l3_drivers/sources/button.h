#pragma once

#include "FreeRTOS.h"
#include "semphr.h"

SemaphoreHandle_t switch0_pressed_signal;
SemaphoreHandle_t switch1_pressed_signal;

void button__initialize(void);