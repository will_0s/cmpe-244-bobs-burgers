
#include "joystick.h"
#include "adc.h"
#include "board_io.h"
#include "lpc40xx.h"

void joystick__initialize(void) {
  LPC_IOCON->P0_25 &= ~(1 << 7);
  LPC_IOCON->P1_30 &= ~(1 << 7);

  gpio__construct_with_function(GPIO__PORT_0, 25, GPIO__FUNCTION_1);
  gpio__construct_with_function(GPIO__PORT_1, 30, GPIO__FUNCTION_1);

  adc__initialize();
}

float joystick__get_reading(joystick__number joystick) {
  const uint16_t ADC_MAX_COUNT = 4096;

  uint16_t adc_count = 0;

  adc_channel_e joystick_adc_channel;
  if (joystick == JOYSTICK__1) {
    joystick_adc_channel = ADC__CHANNEL_2;
  } else if (joystick == JOYSTICK__2) {
    joystick_adc_channel = ADC__CHANNEL_4;
  }
  adc_count = adc__get_adc_value(joystick_adc_channel);

  float adc_percentage = 0;

  adc_percentage = ((float)adc_count - ((float)ADC_MAX_COUNT / 2.0)) / ((float)ADC_MAX_COUNT / 2.0);
  return adc_percentage;
}
